'''This file defines all classes of shapes'''

__author__ = 'Michael Baumann'
__copyright__ = 'Copyright 2016'
__credits__ = ['Gabriel Nuetzi']
__email__ = 'baumann@imes.mavt.ethz.ch'
__status__ = 'Development'

import numpy as np
from numpy import sin, cos
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors as mcolors
from math import sqrt, exp
import random

class Shape:
    '''Main class for all shapes

    Attributes:
        Class Attributes:
            # shape Ids of dynamic classes:
            circShapeId: shapeId of Circle
            squaShapeId: shapeId of Square
            rectShapeId: shapeId of Rectangle
            cycPShapeId: shapeId of CyclicPolygon
            polyShapeId: shapeId of Polygon
            circShapeId: shapeId of Ring
            ringShapeId: shapeId of PolygonizedObject
            maxShapeId: maximal existing shapeId (used in Collider)

            # shape Ids of static classes: (boundary):
            rectCompShapeId: shapeId of RectangleComplement
            circCompShapeId: shapeId of CircleComplement
            ringCompShapeId: shapeId of RingComplement
            pObjCompShapeId: shapeId of PolygonizedObjectComplement

        Instance Attributes:
            shapeId: Id number of shape
            A: area of shape
            pos: position
            col:  color
            i: number of element (counting number of all elements (except boundary) at creation)

    '''
    # shape Ids of dynamic classes:
    dynamicShapeIds = {'circShapeId': 0,
                       'squaShapeId': 1,
                       'rectShapeId': 2,
                       'cycPShapeId': 3,
                       'polyShapeId': 4,
                       'ringShapeId': 5,
                       'pObjShapeId': 6}
    locals().update(dynamicShapeIds)
    
    # shape Ids of static classes: (boundary):
    staticShapeIds = {'rectCompShapeId': -1,
                      'circCompShapeId': -2,
                      'ringCompShapeId': -3,
                      'pObjCompShapeId': -4}
    locals().update(staticShapeIds)
    
    def __init__(self,shapeId,A,i,pos):
        '''

        Args:
            shapeId: Id number of shape
            A: area of shape
            i: number of element (counting number of all elements (except boundary) at creation)
            pos: position
            
        '''
        self.shapeId = shapeId
        self.A = A
        self.i = i
        self.pos = np.array(pos)
        self.col = None # (unknown when instance is created)

class Circle(Shape):
    '''Circle (dynamic element)

    Attributes:
        r: radius
        colorId: colorId for plotting

    '''
    def __init__(self,A,i,colorIdCircle,pos=None):
        '''

        Args:
            A: area of shape
            i: number of element (counting number of all elements (except boundary) at creation)
            colorIdCircle: colorId (circle-specific) for plotting
            pos: position
            
        '''
        Shape.__init__(self,self.circShapeId,A,i,pos)
        self.r = sqrt(A/np.pi) # radius
        self.colorIdCircle = colorIdCircle

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
        '''
        # flat: uniform color
        # colormap: radially changing color
        if self.colorIdCircle == 'flat':
            ax.add_patch(patches.Circle(self.pos,
                              self.r, 
                              facecolor=self.col, 
                              edgecolor='none')) # clip_on=T/F?
        elif self.colorIdCircle == 'colormap':
            alpha = 0.5
            color  = [alpha*np.array([0,0,0])  + (1-alpha)*np.array(self.col), # inner
                      alpha*np.array(self.col) + (1-alpha)*np.array(self.col), # middle
                      alpha*np.array([1,1,1])  + (1-alpha)*np.array(self.col)] # outer
            colorpos = [0,0.5,1] # position at which color is defined

            cdict={}
            for idx,RGBcolor in enumerate(['red','green','blue']):
                cdict[RGBcolor] = [(colorpos[i],)+2*(color[i][idx],) for i in range(0,3)]

            colormap = mpl.colors.LinearSegmentedColormap('my_colormap', cdict)
            resolution = 2000
            threshold = 1

            xgrid, ygrid = np.mgrid[-self.r:self.r:2/resolution,-self.r:self.r:2/resolution]
            image=threshold/self.r*np.sqrt(xgrid**2+ygrid**2)
            image[image>=threshold]=np.nan
            image=np.exp(-.5*image**2)
            plt.imshow(image.T, cmap=colormap, extent=[self.pos[0]-self.r,self.pos[0]+self.r,self.pos[1]-self.r,self.pos[1]+self.r],zorder=1)

class Square(Shape):
    '''Square (dynamic element)

    Attributes:
        d: edge length

    '''
    def __init__(self,A,i,pos=None):
        '''

        Args:
            A: area of shape
            i: number of element (counting number of all elements (except boundary) at creation)
            pos: position
            
        '''
        Shape.__init__(self,self.squaShapeId,A,i,pos) 
        self.d = sqrt(A) # edge

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
        '''
        ax.add_patch(patches.Rectangle(self.pos-self.d/2,
                                       self.d,self.d,
                                       edgecolor="none",
                                       facecolor=self.col))

    def convertToRectangle(self):
        '''convert square into a rectangle

        Args:
            None

        Returns:
            self
            
        '''
        self.__class__ = Rectangle
        self.shapeId = self.rectShapeId
        self.w = self.d # width
        self.h = self.d # heigth
        del self.d
        return self

class Rectangle(Shape):
    '''Rectangle (dynamic element)

    Note: Rectangle with ratio=1/2 corresponds to a Square

    Attributes:
        w: width
        h: heigth

    '''
    def __init__(self,A,i,ratio,pos=None):
        '''

        Args:
            A: area of shape
            i: number of element (counting number of all elements (except boundary) at creation)
            ratio: width/height = ratio**2/(1-ratio)**2
            pos: position
            
        '''
        Shape.__init__(self,self.rectShapeId,A,i,pos)
        self.w = sqrt(A)*ratio/(1-ratio) # width
        self.h = sqrt(A)*(1-ratio)/ratio # heigth

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
        '''
        ax.add_patch(patches.Rectangle((self.pos[0]-self.w/2,self.pos[1]-self.h/2),
                                       self.w,self.h,
                                       edgecolor="none",
                                       facecolor=self.col))

    def convertToCyclicPolygon(self):
        '''convert rectangle into a cyclic polygon

        Args:
            None

        Returns:
            self
            
        '''
        self.__class__ = CyclicPolygon
        self.shapeId = self.cycPShapeId
        self.N = 4 # number of vertices

        self.vertices = np.array([[self.w/2, self.h/2],[-self.w/2, self.h/2],[-self.w/2, -self.h/2],[self.w/2, -self.h/2]])
        self.r = sqrt(self.w**2+self.h**2)/2

        del self.w,self.h
        return self

class CyclicPolygon(Shape):
    '''Cyclic Polygon (dynamic element)

    Note: A cyclic polygon is a polygon for which a circumcircle exists
          includes also all regular polygon such as regular triangle or square

    Attributes:
        vertices: array of all vertices
        r: radius of circumcircle

    '''
    def __init__(self,A,i,N,angle,acutenessfactor,pos=None):
        '''

        Args:
            A: area of shape
            i: number of element (counting number of all elements (except boundary) at creation)
            N: number of vertices
            angle: angle of (a) vertex, counterclockwise, starting from top
            pos: position
                     
        '''
        Shape.__init__(self,self.cycPShapeId,A,i,pos)
        # generate random CyclicPolygon with N vertices
        angles = []
        for i in range(0,N):
            angles.append(acutenessfactor*2*np.pi*random.random()) # counterclockwise starting at top
        angles.sort()
        angles = np.add(angles,[(1-acutenessfactor)*2*np.pi*i/N for i in range(0,N)])
        angles += angle
        self.vertices = np.array([[-sin(angle), cos(angle)] for angle in angles])

        # determine the radius given the area
        # Atot = r^2 * Aunit
        # Aunit calculated by shoelace formula
        x0 = self.vertices[:,0]
        x1 = np.roll(x0,-1)
        y0 = self.vertices[:,1]
        y1 = np.roll(y0,-1)
        Aunitsigned=0.5*(np.dot(x0,y1)-np.dot(x1,y0)) # signed Area / shoelace formula
        self.r = sqrt(A/np.abs(Aunitsigned))
        self.vertices *= self.r

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
        '''
        ax.add_patch(patches.Polygon(self.pos+self.vertices,
                                     True, # polygon is closed
                                     edgecolor="none",
                                     facecolor=self.col))

class Polygon(Shape):
    '''Polygon (dynamic element)

    Note: Not yet tested!

    Attributes:
        vertices: array of all vertices
        posBC: position of center of bounding circle
        r: radius of bounding circle

    '''
    def __init__(self,i,vertices,pos=None):
        '''

        Args:
            i: number of element (counting number of all elements (except boundary) at creation)
            vertices: list of vertices
            pos: position
                     
        '''
        self.vertices = np.array(vertices)
        x0 = vertices[:,0]
        x1 = np.roll(x0,-1)
        y0 = vertices[:,1]
        y1 = np.roll(y0,-1)
        Asigned=0.5*(np.dot(x0,y1)-np.dot(x1,y0)) # signed Area / shoelace formula
        Cx = 1/(6*Asigned)*sum((x0+x1)*(x0*y1-x1*y0)) # x-coordinate of centroid
        Cy = 1/(6*Asigned)*sum((y0+y1)*(x0*y1-x1*y0)) # x-coordinate of centroid
        self.posBC = np.array([Cx,Cy])
        self.r = max(np.linalg.norm(vertices-self.posBC,axis=1))
        Shape.__init__(self,self.polyShapeId,np.abs(Asigned),i,pos)

    def plot(self,fig,ax):
        ax.add_patch(patches.Polygon(self.pos+self.vertices,
                                     True, # polygon is closed
                                     edgecolor='none',
                                     facecolor=self.col))

class Ring(Shape):
    '''Ring (dynamic element)

    Note: circle in a circle

    Attributes:
        ro: outer radius
        ri: inner radius

    '''
    def __init__(self,A,i,ratio,pos=None): # ratio = ri/ro \in [0,1)
        '''

        Args:
            A: area
            i: number of element (counting number of all elements (except boundary) at creation)
            ratio: ratio of inner and outer radii (ratio = ri/ro)
            pos: position
                     
        '''
        Shape.__init__(self,self.ringShapeId,A,i,pos)
        self.ro = sqrt(A/(np.pi*(1-ratio**2))) # outer radius
        self.ri = ratio*self.ro # inner radius

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
            '''
        ax.add_patch(patches.Wedge(self.pos,
                                   self.ro,
                                   0,
                                   360, #degrees
                                   width = self.ro-self.ri,
                                   facecolor=self.col, 
                                   edgecolor='none'))

class PolygonizedObject(Shape):
    '''Polygonized object (dynamic element)

    Attributes:
        r: radius of bounding circle (center of bounding circle is at the origin)
        polygons: list of polygons used for collision detection
        polygonsDraw: list of polygons for drawing
        pos: position

    '''
    
    # object poligonized using triangles
    
    def __init__(self,A,i,pObjDict,pos=None):
        '''

        Args:
            A: area
            i: number of element (counting number of all elements (except boundary) at creation)
            pObjDict: dict for polygonized object
            pos: position
                     
        '''
        Shape.__init__(self,self.pObjShapeId,A,i,pos)

        if pObjDict.__contains__('polygonsApprox'): # appxomimating polygons exist
            area = pObjDict['areaApprox']
            scale = sqrt(A/area) # get scale factor such that it has the desired area
            #self.posBC = np.array(pObjDict['centerBCApprox']) # position of center of bounding circle
            self.r = pObjDict['radiusBCApprox']*scale
            self.polygons = [scale*(np.array(polygon)-pObjDict['centerBCApprox']) for polygon in pObjDict['polygonsApprox']] # shift and scale object such that it has the desired area  and is centered at the origin
            self.polygonsDraw = [scale*(np.array(polygon)-pObjDict['centerBCApprox']) for polygon in pObjDict['polygons']]
        else:
            area = pObjDict['area']
            scale = sqrt(A/area) # get scale factor such that it has the desired area
            #self.posBC = np.array(pObjDict['centerBC']) # position of center of bounding circle
            self.r = pObjDict['radiusBC']*scale
            self.polygons = [scale*(np.array(polygon)-pObjDict['centerBC']) for polygon in pObjDict['polygons']] # shift and scale object such that it has the desired area and is centered at the origin
            self.polygonsDraw = self.polygons

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
        '''
        SequentialColorMaps =['Blues', 'BuGn', 'BuPu', 'GnBu', 'Greens', 'Greys', 'Oranges', 'OrRd', 'PuBu', 'PuBuGn', 'PuRd', 'Purples', 'RdPu', 'Reds', 'YlGn', 'YlGnBu', 'YlOrBr', 'YlOrRd']
        colormap = plt.cm.get_cmap(random.choice(SequentialColorMaps))
        for polygon in self.polygonsDraw:
            ax.add_patch(patches.Polygon(self.pos+polygon,
                                 True,
                                 edgecolor='none',
                                 facecolor=colormap(random.random())[0:3]))
        #ax.add_patch(patches.Circle(self.pos,
        #                  self.r, 
        #                  facecolor='none', 
        #                  edgecolor='k',
        #                  linewidth=0.1,))

# static child classes:
class RectangleComplement(Shape):
    '''Rectangle (static element)

    Note: Complement because it is a static element and other dynamic shapes are placed inside

    Attributes:
        w: width of rectangle
        h: height of rectangle

    '''

    def __init__(self,w,h,pos=[0,0]):
        '''

        Args:
            w: width of rectangle
            h: height of rectangle
            pos: position
                     
        '''
        self.w = w
        self.h = h
        A = w*h
        col = None
        Shape.__init__(self,self.rectCompShapeId,A,col,pos) 

    def repositionShapeInside(self,shape):
        '''position dynamic shape inside static shape

        Args:
            shape: shape to be placed

        Returns:
            None
            
        '''
        if shape.shapeId == self.circShapeId: #Circle
            shape.pos = np.array([(self.w-2*shape.r)*(random.random()-0.5),(self.h-2*shape.r)*(random.random()-0.5)])
        elif shape.shapeId == self.squaShapeId: #Square
            shape.pos = np.array([(self.w-shape.d)*(random.random()-0.5),(self.h-shape.d)*(random.random()-0.5)])
        elif shape.shapeId == self.rectShapeId: #Rectangle
            shape.pos = np.array([(self.w-shape.w)*(random.random()-0.5),(self.h-shape.h)*(random.random()-0.5)])
        elif shape.shapeId == self.cycPShapeId or shape.shapeId == self.polyShapeId: #CyclicPolygon or Polygon
            xmin=shape.vertices[:,0].min()
            xmax=shape.vertices[:,0].max()
            ymin=shape.vertices[:,1].min()
            ymax=shape.vertices[:,1].max()
            shape.pos = np.array([-(xmin+xmax)/2+(self.w-xmax+xmin)*(random.random()-0.5),
                                  -(ymin+ymax)/2+(self.w-ymax+ymin)*(random.random()-0.5)])
        elif shape.shapeId == self.ringShapeId: #Ring
            shape.pos = np.array([(self.w-2*shape.ro)*(random.random()-0.5),(self.h-2*shape.ro)*(random.random()-0.5)])
        elif shape.shapeId == self.pObjShapeId: #PolygonizedObject
            xmin=min([polygon[:,0].min() for polygon in shape.polygons])
            xmax=max([polygon[:,0].max() for polygon in shape.polygons])
            ymin=min([polygon[:,1].min() for polygon in shape.polygons])
            ymax=max([polygon[:,1].max() for polygon in shape.polygons])
            shape.pos = np.array([-(xmin+xmax)/2+(self.w-xmax+xmin)*(random.random()-0.5),
                                  -(ymin+ymax)/2+(self.w-ymax+ymin)*(random.random()-0.5)])

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
        '''
        ax.add_patch(patches.Rectangle((self.pos[0]-self.w/2,self.pos[1]-self.h/2),
                                       self.w,self.h,
                                       edgecolor='none',
                                       facecolor=self.col))

class CircleComplement(Shape):
    '''Circle (static element)

    Note: Complement because it is a static element and other dynamic shapes are placed inside

    Attributes:
        r: radius of circle

    '''
    def __init__(self,r,pos=[0,0]):
        '''

        Args:
            r: radius of circle
            pos: position
                     
        '''
        A = np.pi*r**2
        col = None
        Shape.__init__(self,self.circCompShapeId,A,col,pos) 
        self.r = r

    def repositionShapeInside(self,shape):
        '''position dynamic shape inside static shape

        Args:
            shape: shape to be placed

        Returns:
            None
            
            '''
        
        if shape.shapeId == self.circShapeId: #Circle
            while 1:
                shape.pos = np.array([2*(self.r-shape.r)*(random.random()-0.5),2*(self.r-shape.r)*(random.random()-0.5)])
                if self.r-shape.r > np.linalg.norm(shape.pos):
                    break
        elif shape.shapeId == self.squaShapeId: #Square
            while 1:
                shape.pos = np.array([2*(self.r-shape.d/2)*(random.random()-0.5),2*(self.r-shape.d/2)*(random.random()-0.5)])
                if self.r > max([np.linalg.norm(np.add(shape.pos,shape.d/2*np.array(ind))) for ind in [[-1,-1],[-1,1],[1,-1],[1,1]]]):
                    break
        elif shape.shapeId == self.rectShapeId: #Rectangle
            while 1:
                shape.pos = np.array([2*(self.r-shape.w/2)*(random.random()-0.5),2*(self.r-shape.h/2)*(random.random()-0.5)])
                if self.r > max([np.linalg.norm(np.add(shape.pos,1/2*np.array(ind))) for ind in [[-shape.w,-shape.h],[-shape.w,shape.h],[shape.w,-shape.h],[shape.w,shape.h]]]):
                    break
        elif shape.shapeId == self.cycPShapeId or shape.shapeId == self.polyShapeId: #CyclicPolygon or Polygon
            while 1:
                shape.pos = np.array([2*self.r*(random.random()-0.5),2*self.r*(random.random()-0.5)])
                if self.r > max([np.linalg.norm(np.add(shape.pos,vertex)) for vertex in shape.vertices]):
                    break
        elif shape.shapeId == self.ringShapeId: #Ring
            while 1:
                shape.pos = np.array([2*(self.r-shape.ro)*(random.random()-0.5),2*(self.r-shape.ro)*(random.random()-0.5)])
                if self.r-shape.ro > np.linalg.norm(shape.pos):
                    break
        elif shape.shapeId == self.pObjShapeId: #PolygonizedObject
            while 1:
                shape.pos = np.array([2*self.r*(random.random()-0.5),2*self.r*(random.random()-0.5)])
                if self.r > max([max([np.linalg.norm(np.add(shape.pos,vertex)) for vertex in polygon]) for polygon in shape.polygons]):
                    break

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
            '''
        ax.add_patch(patches.Circle(self.pos,
                                    self.r,
                                    edgecolor='none',
                                    facecolor=self.col))

class RingComplement(Shape):
    '''Ring (static element)

    Note: Complement because it is a static element and other dynamic shapes are placed inside

    Attributes:
        ro: outer radius of ring
        ri: inner radius of ring

    '''
    def __init__(self,ro,ri,pos=[0,0]):
        '''

        Args:
            ro: outer radius of ring
            ri: inner radius of ring
            pos: position
                     
        '''
        self.ro = ro
        self.ri = ri
        A = np.pi*(ro**2-ri**2)
        col = None        
        Shape.__init__(self,self.ringCompShapeId,A,col,pos) 
        

    def repositionShapeInside(self,shape):
        '''position dynamic shape inside static shape

        Args:
            shape: shape to be placed

        Returns:
            None
            
        '''
        if shape.shapeId == self.circShapeId: #Circle
            while 1:
                shape.pos = np.array([2*(self.ro-shape.r)*(random.random()-0.5),2*(self.ro-shape.r)*(random.random()-0.5)])
                if self.ro-shape.r > np.linalg.norm(shape.pos) and self.ri+shape.r < np.linalg.norm(shape.pos):
                    break
        elif shape.shapeId == self.squaShapeId: #Square
            pass
#            while 1:
#                shape.pos = np.array([2*(self.r-shape.d/2)*(random.random()-0.5),2*(self.r-shape.d/2)*(random.random()-0.5)])
#                if self.r > max([np.linalg.norm(np.add(shape.pos,shape.d/2*np.array(ind))) for ind in [[-1,-1],[-1,1],[1,-1],[1,1]]]):
#                    break
        elif shape.shapeId == self.rectShapeId: #Rectangle
            pass
#            while 1:
#                shape.pos = np.array([2*(self.r-shape.w/2)*(random.random()-0.5),2*(self.r-shape.h/2)*(random.random()-0.5)])
#                if self.r > max([np.linalg.norm(np.add(shape.pos,1/2*np.array(ind))) for ind in [[-shape.w,-shape.h],[-shape.w,shape.h],[shape.w,-shape.h],[shape.w,shape.h]]]):
#                    break
        elif shape.shapeId == self.cycPShapeId or shape.shapeId == self.polyShapeId: #CyclicPolygon or Polygon
            pass
#            while 1:
#                shape.pos = np.array([2*self.r*(random.random()-0.5),2*self.r*(random.random()-0.5)])
#                if self.r > max([np.linalg.norm(np.add(shape.pos,vertex)) for vertex in shape.vertices]):
#                    break
        elif shape.shapeId == self.ringShapeId: #Ring
            while 1:
                shape.pos = np.array([2*(self.ro-shape.ro)*(random.random()-0.5),2*(self.ro-shape.ro)*(random.random()-0.5)])
                if self.ro-shape.ro > np.linalg.norm(shape.pos) and self.ri+shape.ro < np.linalg.norm(shape.pos):
                    break
        elif shape.shapeId == self.pObjShapeId: #PolygonizedObject
            pass
#            while 1:
#                shape.pos = np.array([2*self.r*(random.random()-0.5),2*self.r*(random.random()-0.5)])
#                if self.r > max([max([np.linalg.norm(np.add(shape.pos,vertex)) for vertex in polygon]) for polygon in shape.polygons]):
#                    break

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
        '''
        ax.add_patch(patches.Wedge(self.pos,
                                   self.ro,
                                   0,
                                   360, #degrees
                                   width = self.ro-self.ri,
                                   edgecolor='none',
                                   facecolor=self.col))

class PolygonizedObjectComplement(Shape):
    '''polygonized object (static element)

    Note: Complement because it is a static element and other dynamic shapes are placed inside

    Attributes:
        pObjDict: dictionary of polygonized object
        polygons: list of polygons
        xlim: min/max of x-values of all vertices
        ylim: min/max of y-values of all vertices

    '''
    def __init__(self,pObjDict,pos=[0,0]):
        '''

        Args:
            pObjDict: dictionary of polygonized object
            pos: position
                     
        '''
        A = pObjDict['areaApprox']-pObjDict['area']
        col = None
        Shape.__init__(self,self.pObjCompShapeId,A,col,pos) 
        self.pObjDict = pObjDict
        self.polygons = [np.array(polygon) for polygon in pObjDict['polygons']]

        lim = min(pObjDict['polygonsApprox'][0]) + max(pObjDict['polygonsApprox'][0])
        self.xlim = [lim[0],lim[2]]
        self.ylim = [lim[1],lim[3]]


    def repositionShapeInside(self,shape):
        '''position dynamic shape inside static shape

        Args:
            shape: shape to be placed

        Returns:
            None
            
            '''
        
        if shape.shapeId == self.circShapeId: #Circle
            maxit = 1000
            it = 0
            while it<maxit:
                it+=1
                shape.pos = np.array([(self.xlim[1]-self.xlim[0]-2*shape.r)*random.random()+self.xlim[0]+shape.r,
                                      (self.ylim[1]-self.ylim[0]-2*shape.r)*random.random()+self.ylim[0]+shape.r])

                for poly in self.polygons:
                    collision = 1
                    # test normal directions (normal to edges) (-> no collision)
                    normals = np.column_stack((np.roll(poly[:,1],-1)-poly[:,1],-(np.roll(poly[:,0],-1)-poly[:,0])))
                    normals = [normal/np.linalg.norm(normal) for normal in normals]
                    for normal in normals:
                        polyprojected = np.dot(normal,np.add(self.pos,poly).T)
                        circcenterprojected = np.dot(normal,shape.pos.T)
                        if polyprojected.min()>circcenterprojected+shape.r or polyprojected.max()<circcenterprojected-shape.r:
                            collision = 0 # no collision (yet)
                            break
                    if collision:
                        # test normal directions (normal to connection between vertices and center of circle) (-> no collision)
                        normals = np.column_stack((poly[:,1]-shape.pos[1],-(poly[:,0]-shape.pos[0])))
                        normals = [normal/np.linalg.norm(normal) for normal in normals]
                        for normal in normals:
                            polyprojected = np.dot(normal,np.add(self.pos,poly).T)
                            circcenterprojected = np.dot(normal,shape.pos.T)
                            if polyprojected.min()>circcenterprojected+shape.r or polyprojected.max()<circcenterprojected-shape.r:
                                collision = 0 # no collision (yet)
                                break
                    if collision: # start over!
                        break
                else: # no collision: done: exit!
                    break
                #continue
        elif shape.shapeId == self.squaShapeId: #Square
            pass
        elif shape.shapeId == self.rectShapeId: #Rectangle
            pass
        elif shape.shapeId == self.cycPShapeId or shape.shapeId == self.polyShapeId: #CyclicPolygon or Polygon
            pass
        elif shape.shapeId == self.ringShapeId: #Ring
            pass
        elif shape.shapeId == self.pObjShapeId: #PolygonizedObject
            pass

    def plot(self,fig,ax):
        '''plot itself

        Args:
            fig: figure
            ax: axis

        Returns:
            None
            
            '''
        pass
#        for polygon in self.polygons:
#            ax.add_patch(patches.Polygon(self.pos+polygon,
#                                 True,
#                                 edgecolor='none',
#                                 facecolor='blue'))