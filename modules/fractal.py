'''This module defines the class Fractal'''

__author__ = 'Michael Baumann'
__copyright__ = 'Copyright 2016'
__credits__ = ['Gabriel Nuetzi']
__email__ = 'baumann@imes.mavt.ethz.ch'
__status__ = 'Development'

import numpy as np
from scipy.special import zeta
import random
import random
import math

from modules.shapes import *
from modules.collider import Collider
from modules.colorMaker import ColorMaker

class Fractal:
    '''Main class for creating the statistically fractal images

    Attributes:
        boundary: boundary object within all objects are placed
        shapelist: reference onto the list of all shapes
        n: counter of all shapes (=len(shapelist))
        collider: Instance of Collider class for collision detection
        colormaker: Instance of Colormaker class for all coloring purposes
        fig: figure handle
        ax: axis handle
        iteration_text: text printed on image
            [*args: Variable length argument list.]
        **kwargs: Arbitrary keyword arguments

    '''
    def __init__(self,boundaryOpts,colorOpts,**kwargs): # kwargs = generalOpts
        '''

        Args:
            boundary: boundary object within all objects are placed
            colorOpts: options for coloring
            **kwargs: Arbitrary keyword arguments

        '''
        
        self.shapelist = []
        self.n = 0
    
        # read all parameters from dictionary pardict and store as attribute in (self.)p
        for k,v in kwargs.items():
            setattr(self,k,v)
        
        # create boundary
        if boundaryOpts['ShapeId'] == Shape.rectCompShapeId:
            self.boundary = RectangleComplement(boundaryOpts['w'],boundaryOpts['h'],boundaryOpts['pos'])
        elif boundaryOpts['ShapeId'] == Shape.circCompShapeId:
            self.boundary = CircleComplement(boundaryOpts['r'],boundaryOpts['pos'])
        if boundaryOpts['ShapeId'] == Shape.ringCompShapeId:
            self.boundary = RingComplement(boundaryOpts['ro'],boundaryOpts['ri'],boundaryOpts['pos'])
        if boundaryOpts['ShapeId'] == Shape.pObjCompShapeId:
            self.boundary = PolygonizedObjectComplement(boundaryOpts['pObjDict'],boundaryOpts['pos'])
        
        # define getAfun
        if self.seriesOpts['type'] == 'Dirichlet':
            # Ai = A0*(1+shift)**exponent / (i+1+shift)**exponent
            # sum(Ai) = A0*zeta(s,1+shift)
            # sum(Ai) != Atot*totalCoverage
            totalCoverage = self.seriesOpts['totalCoverage']
            exponent = self.seriesOpts['exponent']
            shift = self.seriesOpts['shift']
            A0 = self.boundary.A*totalCoverage/zeta(exponent,1+shift)
            self.getAfun = lambda i : A0*(1+shift)**exponent / (i+1+shift)**exponent
        elif self.seriesOpts['type'] == 'geometric':
            # Ai = A0*ratio**i
            # sum(Ai) = A0/(1-ratio)
            # sum(Ai) != Atot*totalCoverage
            totalCoverage = self.seriesOpts['totalCoverage']
            geomConst = self.seriesOpts['geomConst']
            A0 = self.boundary.A*totalCoverage*(1-geomConst)
            self.getAfun = lambda i : A0*geomConst**i
        elif self.seriesOpts['type'] == 'constant':
            # Ai = A0
            # sum(Ai) = A0*N
            # sum(Ai) != Atot*totalCoverage
            totalCoverage = self.seriesOpts['totalCoverage']
            constantN = self.seriesOpts['constantN']
            A0 = self.boundary.A*totalCoverage/constantN
            self.getAfun = lambda i : A0
        else: print('series {}} not recognized'.format(self.seriesOpts['type']))

        # make Collider
        self.collider = Collider(self.shapelist)
        
        # make ColorMaker
        self.colormaker = ColorMaker(self.shapelist,self.boundary,colorOpts)
        
        # figure
        self.fig, self.ax = plt.subplots() # note we must use plt.subplots, not plt.subplot
        plt.axis('equal')
        plt.axis([self.windowsize[0]-self.windowsize[2]/2,self.windowsize[0]+self.windowsize[2]/2,self.windowsize[1]-self.windowsize[3]/2,self.windowsize[1]+self.windowsize[3]/2])
        plt.axis('off')
        #self.iteration_text = self.ax.text(0.5, -0.05,'',transform=self.ax.transAxes,horizontalalignment='center')
        
        # plot boundary
        self.colormaker.makecolor(self.boundary)
        self.boundary.plot(self.fig,self.ax)
        
    def push(self,**kwargs):
        '''add an additional shape to shapelist
        
        Args:
            **kwargs: Arbitrary keyword arguments
            
        Returns:
            1 if error occured, 0 otherwise
        
        '''
        shapeId = kwargs.get('shapeId')
        if isinstance(shapeId, str): shapeId = eval(shapeId)
        A = self.getAfun(self.n)
        if shapeId is Shape.circShapeId:
            colorIdCircle = kwargs.get('colorIdCircle','flat')
            newshape = Circle(A,self.n,colorIdCircle)
        elif shapeId is Shape.squaShapeId:
            newshape = Square(A,self.n)
        elif shapeId is Shape.rectShapeId:
            ratio = kwargs.get('ratio',random.uniform(0.4,0.6))
            if isinstance(ratio, str): ratio = eval(ratio)
            newshape = Rectangle(A,self.n,ratio) # ratio
        elif shapeId is Shape.cycPShapeId:
            N = kwargs.get('N',random.randrange(3,7))# number of vertices (3+)
            if isinstance(N, str): N = eval(N)
            angle = kwargs.get('angle',2*np.pi*(random.random())/24)
            if isinstance(angle, str): angle = eval(angle)
            acutenessfactor = kwargs.get('acutenessfactor',0) # in [0,1]: 0: regular, 1:no restrictions
            newshape = CyclicPolygon(A,self.n,N,angle,acutenessfactor) 
        elif shapeId is Shape.polyShapeId:
            pass #Polygon
        elif shapeId is Shape.ringShapeId:
            ratio = kwargs.get('ratio',random.uniform(0,0.7))
            if isinstance(ratio, str): ratio = eval(ratio)
            newshape = Ring(A,self.n,ratio)
        elif shapeId is Shape.pObjShapeId:
            pObjDict = kwargs.get('pObjDict') # no deault value
            newshape = PolygonizedObject(A,self.n,pObjDict)
  
        iter = 0
        while 1:
            self.boundary.repositionShapeInside(newshape)
            if not self.collider.detectCollision(newshape):
                break
            iter+=1
            if iter == self.maxIter: return 1 # not converged after maxIter iterations

        self.colormaker.makecolor(newshape)
        
        self.shapelist.append(newshape)
        self.n += 1
        return 0
    
    def plot(self,ndraw=None):
        '''plot the last ndraw shapes in shapelist (all by default)
        
        Args:
            ndraw: number of shapes to draw (all by default)
            
        Returns:
           None
        
        '''
        if ndraw is None: ndraw = self.n
        for idx in range(-ndraw,0):
            self.shapelist[self.n+idx].plot(self.fig,self.ax)
            #self.iteration_text.set_text('iteration: %i / colorId: %i' % (self.n+idx+1,self.colormaker.colorOpts['colorId']))
        plt.pause(0.000001)
        
    # close plot
    def close(self):
        '''close the plot
        
        Args:
            None
            
        Returns:
           None
        
        '''
        #plt.waitforbuttonpress()
        plt.close()