'''List of predefined parameter sets for fractals

shape Ids of possible shapes: see class Shape
color Ids of possible colorstyles: see class ColorMaker

'''

__author__ = 'Michael Baumann'
__copyright__ = 'Copyright 2016'
__credits__ = ['Gabriel Nuetzi']
__email__ = 'baumann@imes.mavt.ethz.ch'
__status__ = 'Development'

import json # JavaScript Object Notation
import os.path # for creating paths names using join

from modules.shapes import *
from modules.colorMaker import ColorMaker

## parameter sets
parameterSetList={}

# generalOpts for circles in square
design = 'circlesInSquare'
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 1,
              'exponent': 1.27,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000}
boundaryOpts = {'ShapeId': Shape.rectCompShapeId, 'w': 1, 'h': 1, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.randomPaletteColorId}
shapeOpts = {'shapeId': Shape.circShapeId, 'colorIdCircle': 'colormap'}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'shapeOpts': shapeOpts, 'colorOpts': colorOpts}

# generalOpts for sparse squares in square
design = 'sparseSquaresInSquare'
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 0.2,
              'exponent': 1.05,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000} 
boundaryOpts = {'ShapeId': Shape.rectCompShapeId, 'w': 1, 'h': 1, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.uniformColorId,'backgroundColor': [0,0,0], 'shapeColor': [1,1,1]}     
shapeOpts = {'shapeId': Shape.squaShapeId}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for rectangles in circle
design = 'rectanglesInCircle'
seriesOpts = { 'type': 'geometric',
               'totalCoverage': 0.7,
               'geomConst': 0.9}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000}
boundaryOpts = {'ShapeId': Shape.circCompShapeId, 'r': 1/4, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.linearMapColorId, 'colorList': [(0,[0.03, 0.19, 0.42]),(1,[1,1,1])]}
shapeOpts = {'shapeId': Shape.rectShapeId}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for red squares
design = 'redSquares'
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 1,
              'exponent': 1.15,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000} 
boundaryOpts = {'ShapeId': Shape.rectCompShapeId, 'w': 1, 'h': 1, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.nonlinearMapColorId}
shapeOpts = {'shapeId': Shape.cycPShapeId, 'N': 4 , 'acutenessfactor': 0, 'angle': '2*np.pi*(random.random())/24'}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for conglomerate
design = 'conglomerate'
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 1,
              'exponent': 1.1,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000} 
boundaryOpts = {'ShapeId': Shape.rectCompShapeId, 'w': 1, 'h': 1, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.randomColorId}
shapeOpts = {'shapeId': Shape.cycPShapeId, 'N': 'math.floor(random.expovariate(0.8))+4' , 'acutenessfactor': 0.5}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for random
design = 'random'
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 1,
              'exponent': 1.3,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000,
               'nMax' : 50} 
boundaryOpts = {'ShapeId': Shape.rectCompShapeId, 'w': 1, 'h': 1, 'pos': [0,0]}
shapeOpts = {'shapeId': 'random.choice([Shape.circShapeId,Shape.squaShapeId,Shape.rectShapeId,Shape.cycPShapeId])', 'N': 'random.randrange(3,7)' , 'acutenessfactor': 0.5}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for rings
design = 'rings'
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 1,
              'exponent': 1.15,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000} 
boundaryOpts = {'ShapeId': Shape.circCompShapeId, 'r': 1/4, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.linearMapColorId}
shapeOpts = {'shapeId': Shape.ringShapeId, 'ratio':  0.7}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for ringsinring
design = 'ringsinring'
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 1,
              'exponent': 1.03,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000}
boundaryOpts = {'ShapeId': Shape.ringCompShapeId, 'ro': 1/4, 'ri': 1/8, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.uniformColorId,'backgroundColor': [0,0,0], 'shapeColor': [1,1,1]}               
shapeOpts = {'shapeId': Shape.ringShapeId, 'ratio':  0.5}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for chameleon
design = 'chameleon'
filename = os.path.join('polygonizedObject','chameleon','chameleonExact' + '.txt')
#filename = os.path.join('polygonizedObject','chameleon','chameleon' + 'Approx' + '.txt')
with open( filename ) as file:  # 'r' for read is default
    pObjDict = json.load(file)
nMax = 3
#seriesOpts = {'type': 'constant',
#              'totalCoverage': 0.5,
#              'constantN': nMax}
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 1,
              'exponent': 1.15,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 10000,
               'nMax': nMax} 
boundaryOpts = {'ShapeId': Shape.circCompShapeId, 'r': 1/4, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.uniformColorId}
shapeOpts = {'shapeId': Shape.pObjShapeId, 'pObjDict': pObjDict}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for sketch
design = 'sketch' # = objName
filename = os.path.join('polygonizedObject',design,design + '.txt')
with open( filename ) as file:  # 'r' for read is default
    pObjDict = json.load(file)
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 0.5,
              'exponent': 1.05,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0,0,1,1], # xM,yM,width,height
               'maxIter': 100000} 
boundaryOpts = {'ShapeId': Shape.rectCompShapeId, 'w': 1, 'h': 1, 'pos': [0,0]}
colorOpts = {'colorId': ColorMaker.uniformColorId}
shapeOpts = {'shapeId': Shape.pObjShapeId, 'pObjDict': pObjDict}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

# generalOpts for circles in chameleon
design = 'circlesInChameleon'
filename = os.path.join('polygonizedObject','chameleon','chameleonBoundary' + '.txt')
with open( filename ) as file:  # 'r' for read is default
    pObjDict = json.load(file)
nMax = 3
seriesOpts = {'type': 'Dirichlet',
              'totalCoverage': 1,
              'exponent': 1.2,
              'shift': 0}
generalOpts = {'seriesOpts': seriesOpts,
               'windowsize': [0.5,0.4,1,1], # xM,yM,width,height
               'maxIter': 10000,
               'nMax': nMax}
boundaryOpts = {'ShapeId': Shape.pObjCompShapeId, 'pObjDict': pObjDict,'pos': [0,0]}
boundary = PolygonizedObjectComplement(pObjDict,[0,0])
colorOpts = {'colorId': ColorMaker.randomPaletteColorId}
shapeOpts = {'shapeId': Shape.circShapeId}
parameterSetList[design] = {'generalOpts': generalOpts, 'boundaryOpts': boundaryOpts, 'colorOpts': colorOpts, 'shapeOpts': shapeOpts}

#with open('modules/parameterSetList.txt','w') as fileOut:  # 'w' for write (deletes existing file)
#    json.dump(parameterSetList, fileOut,indent=0)