'''This module defines the class Collider'''

__author__ = 'Michael Baumann'
__copyright__ = 'Copyright 2016'
__credits__ = ['Gabriel Nuetzi']
__email__ = 'baumann@imes.mavt.ethz.ch'
__status__ = 'Development'

import numpy as np
from modules.shapes import *

class Collider:
    '''Class containing the shapelist is used for collision detection

    Attributes:
        shapelist: reference onto shape list object
        collisionMatrix: Matrix containing all collision functions

    '''
    def __init__(self, shapelist):
        '''

        Args:
            shapelist: list of all dynamic shapes

        '''
        self.shapelist = shapelist
        
        # build collision function matrix
        self.collisionMatrix=np.tile(self.collMissing,2*(max(Shape.dynamicShapeIds.values())+1,)) # if collision is undefined then call function collMissing
        # circle vs. others
        self.collisionMatrix[Shape.circShapeId,Shape.circShapeId] = self.collCircCirc
        self.collisionMatrix[Shape.circShapeId,Shape.squaShapeId] = lambda circ,squa : self.collCircSqua(circ,squa)
        self.collisionMatrix[Shape.squaShapeId,Shape.circShapeId] = lambda squa,circ : self.collCircSqua(circ,squa)
        self.collisionMatrix[Shape.circShapeId,Shape.rectShapeId] = lambda circ,rect : self.collCircRect(circ,rect)
        self.collisionMatrix[Shape.rectShapeId,Shape.circShapeId] = lambda rect,circ : self.collCircRect(circ,rect)
        self.collisionMatrix[Shape.circShapeId,Shape.cycPShapeId] = lambda circ,cycP : self.collCircPoly(circ,cycP)
        self.collisionMatrix[Shape.cycPShapeId,Shape.circShapeId] = lambda cycP,circ : self.collCircPoly(circ,cycP)
        # Square vs. others
        self.collisionMatrix[Shape.squaShapeId,Shape.squaShapeId] = self.collSquaSqua
        self.collisionMatrix[Shape.squaShapeId,Shape.rectShapeId] = lambda squa,rect : self.collRectRect(squa.convertToRectangle(),rect)
        self.collisionMatrix[Shape.rectShapeId,Shape.squaShapeId] = lambda rect,squa : self.collRectRect(squa.convertToRectangle(),rect)
        self.collisionMatrix[Shape.squaShapeId,Shape.cycPShapeId] = lambda squa,cycP : self.collPolyPoly(squa.convertToRectangle().convertToCyclicPolygon(),cycP)
        self.collisionMatrix[Shape.cycPShapeId,Shape.squaShapeId] = lambda cycP,squa : self.collPolyPoly(squa.convertToRectangle().convertToCyclicPolygon(),cycP)
        # Rectangle vs. others
        self.collisionMatrix[Shape.rectShapeId,Shape.rectShapeId] = self.collRectRect
        self.collisionMatrix[Shape.rectShapeId,Shape.cycPShapeId] = lambda rect,cycP : self.collPolyPoly(rect.convertToCyclicPolygon(),cycP)
        self.collisionMatrix[Shape.cycPShapeId,Shape.rectShapeId] = lambda cycP,rect : self.collPolyPoly(rect.convertToCyclicPolygon(),cycP)
        # CyclicPolygon vs. others
        self.collisionMatrix[Shape.cycPShapeId,Shape.cycPShapeId] = self.collPolyPoly
        # Polygon vs. others
        #self.collisionMatrix[Shape.polyShapeId,Shape.polyShapeId] = self.collPolyPoly
        # Ring vs. others
        self.collisionMatrix[Shape.ringShapeId,Shape.ringShapeId] = self.collRingRing
        # PolygonizedObject vs. others
        self.collisionMatrix[Shape.pObjShapeId,Shape.pObjShapeId] = self.collPObjPObj
        
    def detectCollision(self,newshape):
        '''collision detection
        
        Args:
            newshape: new shape which is tested against existing shapes
            
        Returns:
            True if collision, False otherwise.
        
        '''
        for shape in self.shapelist:
            if self.collisionMatrix[newshape.shapeId][shape.shapeId](newshape,shape):
                return 1
        return 0
    
    
    def collMissing(self,shape1,shape2):
        '''missing collision detection
        
        Args:
            shape1: any shape
            shape2: any shape
            
        Returns:
            False (indication no collision)
        
        '''
        print('collision between %s and %s not implemented' % (shape1.__class__.__name__,shape2.__class__.__name__))
    
    def collCircCirc(self,circ1,circ2):
        '''collision detection circle vs circle
        
        Args:
            circ1: first circle
            circ2: second circle
            
        Returns:
            True if collision, False otherwise.
        
        '''
        if circ1.r+circ2.r > np.linalg.norm(circ1.pos-circ2.pos):
            return 1
        else:
            return 0

    def collCircSqua(self,circ,squa):
        '''collision detection circle vs square
        
        Args:
            circ: circle
            squa: square
            
        Returns:
            True if collision, False otherwise.
        
        '''
        xdist = abs(circ.pos[0]-squa.pos[0])
        ydist = abs(circ.pos[1]-squa.pos[1])
        
        if xdist > circ.r+squa.d/2:
            return 0
        if ydist > circ.r+squa.d/2:
            return 0
        
        if xdist <= squa.d/2:
            return 1
        if ydist <= squa.d/2:
            return 1
        
        if (xdist-squa.d/2)**2+(ydist-squa.d/2)**2 <= circ.r**2: 
            return 1
        else:
            return 0
    
    def collCircRect(self,circ,rect):
        '''collision detection circle vs rectangle
        
        Args:
            circ: circle
            rect: rectangle
            
        Returns:
            True if collision, False otherwise.
        
        '''
        xdist = abs(circ.pos[0]-rect.pos[0])
        ydist = abs(circ.pos[1]-rect.pos[1])
        
        if xdist > circ.r+rect.w/2:
            return 0
        if ydist > circ.r+rect.h/2:
            return 0
        
        if xdist <= rect.w/2:
            return 1
        if ydist <= rect.h/2:
            return 1
        
        if (xdist-rect.w/2)**2+(ydist-rect.h/2)**2 <= circ.r**2: 
            return 1
        else:
            return 0
        
    def collCircPoly(self,circ,poly):
        '''collision detection circle vs Polygon
        
        Args:
            circ: circle
            poly: polygon
            
        Returns:
            True if collision, False otherwise.
        
        '''
        if circ.r+poly.r < np.linalg.norm(circ.pos-poly.pos): # collision detection by bounding circle
            return 0

        # test normal directions (normal to edges) (-> no collision)
        normals = np.column_stack((np.roll(poly.vertices[:,1],-1)-poly.vertices[:,1],
                                -(np.roll(poly.vertices[:,0],-1)-poly.vertices[:,0])))
        normals = [normal/np.linalg.norm(normal) for normal in normals]
        for normal in normals:
            polyprojected = np.dot(normal,np.add(poly.pos,poly.vertices).T)
            circcenterprojected = np.dot(normal,circ.pos.T)
            if polyprojected.min()>circcenterprojected+circ.r or polyprojected.max()<circcenterprojected-circ.r:
                return 0
        # test normal directions (normal to connection between vertices and center of circle) (-> no collision)
        normals = np.column_stack((poly.vertices[:,1]-circ.pos[1],
                                -(poly.vertices[:,0]-circ.pos[0])))
        normals = [normal/np.linalg.norm(normal) for normal in normals]
        for normal in normals:
            polyprojected = np.dot(normal,np.add(poly.pos,poly.vertices).T)
            circcenterprojected = np.dot(normal,circ.pos.T)
            if polyprojected.min()>circcenterprojected+circ.r or polyprojected.max()<circcenterprojected-circ.r:
                return 0
        # else, collision
        return 1

    def collSquaSqua(self,squa1,squa2):
        '''collision detection square vs square
        
        Args:
            squa1: first square
            squa2: second square
            
        Returns:
            True if collision, False otherwise.
        
        '''
        if (squa1.d+squa2.d)/2 > abs(squa1.pos[0]-squa2.pos[0]) and (squa1.d+squa2.d)/2 > abs(squa1.pos[1]-squa2.pos[1]):
            return 1
        else:
            return 0

    def collRectRect(self,rect1,rect2):
        '''collision detection rectangle vs rectangle
        
        Args:
            rect1: first rectangle
            rect2: second rectangle
            
        Returns:
            True if collision, False otherwise.
        
        '''
        if (rect1.w+rect2.w)/2 > abs(rect1.pos[0]-rect2.pos[0]) and (rect1.h+rect2.h)/2 > abs(rect1.pos[1]-rect2.pos[1]):
                    return 1
        else:
            return 0
    
    def collPolyPoly(self,poly1,poly2):
        '''collision detection polygon vs polygon
        
        Args:
            poly1: first polygon
            poly2: second polygon
            
        Returns:
            True if collision, False otherwise.
        
        '''
        if poly1.r+poly2.r < np.linalg.norm(poly1.pos-poly2.pos): # collision detection by bounding circle
            return 0
        
        for poly in (poly1,poly2):
            normals = np.column_stack((np.roll(poly.vertices[:,1],-1)-poly.vertices[:,1],
                                    -(np.roll(poly.vertices[:,0],-1)-poly.vertices[:,0])))
            for normal in normals:
                projected1 = np.dot(normal,np.add(poly1.pos,poly1.vertices).T)
                projected2 = np.dot(normal,np.add(poly2.pos,poly2.vertices).T)
                if projected1.min()>projected2.max() or projected1.max()<projected2.min():
                    return 0        
        return 1
       
    def collRingRing(self,ring1,ring2):
        '''collision detection ring vs ring
        
        Args:
            ring1: first ring
            ring2: second ring
            
        Returns:
            True if collision, False otherwise.
        
        '''
        dist = np.linalg.norm(ring1.pos-ring2.pos)
        if ring1.ro+ring2.ro < dist:
            return 0
        elif ring1.ri-ring2.ro > dist:
            return 0
        elif -ring1.ro+ring2.ri > dist:
            return 0
        else:            
            return 1
        
    def collPObjPObj(self,pObj1,pObj2):
        '''collision detection polygonized object vs polygonized object
        
        Args:
            pObj1: first polygonized object
            pObj2: second polygonized object
            
        Returns:
            True if collision, False otherwise.
        
        '''
        # bounding circle
        if pObj1.r+pObj2.r < np.linalg.norm(pObj1.pos-pObj2.pos):
            return 0
        
        for poly1 in pObj1.polygons:
            for poly2 in pObj2.polygons:
                collision = 1
                normals = np.append(np.column_stack((np.roll(poly1[:,1],-1)-poly1[:,1],-(np.roll(poly1[:,0],-1)-poly1[:,0]))),
                                    np.column_stack((np.roll(poly2[:,1],-1)-poly2[:,1],-(np.roll(poly2[:,0],-1)-poly2[:,0]))),
                                    axis=0)
                for normal in normals:
                    projected1 = np.dot(normal,np.add(pObj1.pos,poly1).T)
                    projected2 = np.dot(normal,np.add(pObj2.pos,poly2).T)
                    if projected1.min()>projected2.max() or projected1.max()<projected2.min():
                        collision = 0
                        break
                if collision:
                    return 1  
        return 0