'''This module defines the class ColorMaker'''

__author__ = 'Michael Baumann'
__copyright__ = 'Copyright 2016'
__credits__ = ['Gabriel Nuetzi']
__email__ = 'baumann@imes.mavt.ethz.ch'
__status__ = 'Development'

import numpy as np
import matplotlib as mpl
from math import sqrt, exp
import random
import json # JavaScript Object Notation
import os.path # for creating paths names using join

from modules.shapes import *

class ColorMaker:
    '''Class containing the shapelist and boundary and is used for coloring

    Attributes:
        Class Attributes:
            # color Ids:
            uniformColorId: colorId for uniform color
            randomColorId: colorId for random color
            randomPaletteColorId: colorId for random choice from color palettes
            nonlinearMapColorId: colorId for nonlinear color map
            linearMapColorId: colorId for linear color map

        Instance Attributes:
            shapelist: reference onto shape list object
            boundary: reference onto boundary object
            colorOpts: options for coloring
            colorPalettes: dictionary with colorpalettes (imported from folder colorPalettes)

    '''

    uniformColorId = 0
    randomColorId = 1
    randomPaletteColorId = 2
    nonlinearMapColorId = 3
    linearMapColorId = 4

    def __init__(self,shapelist,boundary,colorOpts):
        '''

        Args:
            shapelist: list of all dynamic shapes
            boundary: boundary (static object)
            colorOpts: options for coloring

        '''
        self.shapelist = shapelist
        self.boundary = boundary
        self.colorOpts = colorOpts
        self.colorPalettes = {}
        
        # load all colorpalettes strored as txt-files in folder 'colorPalettes'
        for filename in os.listdir('colorPalettes'):
            if filename.endswith('.txt'):
                with open(os.path.join('colorPalettes',filename)) as file:
                    self.colorPalettes[os.path.splitext(filename)[0]] = np.array(json.load(file))/255 # rescale from 0:255 to 0:1            

    def makecolor(self,shape):
        '''Class method for coloring a shape.

        Args:
            shape: Shape to be colored

        Returns:
            None

        '''        
        if self.colorOpts['colorId'] == self.uniformColorId:
            if shape.shapeId in Shape.staticShapeIds.values(): #boundary/background
                shape.col = self.colorOpts.get('backgroundColor',[1,1,1])
            else:
                shape.col = self.colorOpts.get('shapeColor',[0.5,0.5,0.5])

        elif self.colorOpts['colorId'] == self.randomColorId:
            if shape.shapeId in Shape.staticShapeIds.values(): #boundary/background
                shape.col = self.colorOpts.get('backgroundColor',[1,1,1])
            else:
                shape.col = [random.random(),random.random(),random.random()]

        elif self.colorOpts['colorId'] == self.randomPaletteColorId:
            if shape.shapeId in Shape.staticShapeIds.values(): #boundary/background
                shape.col = self.colorOpts.get('backgroundColor',[0,0,1])
            else:
                colorPaletteName = self.colorOpts.get('colorPaletteName','GreenBlueIWantHue')
                shape.col = random.choice(self.colorPalettes[colorPaletteName])

        elif self.colorOpts['colorId'] == self.nonlinearMapColorId:
            if shape.shapeId in Shape.staticShapeIds.values(): #boundary/background
                shape.col = self.colorOpts.get('backgroundColor',[0,0,0.6])
            else:
                alpha = 1/(shape.i+1)**0.2 #exp(-shape.i/50)
                colorBegin = self.colorOpts.get('colorBegin',[0.9,0,0])
                colorEnd = self.colorOpts.get('colorEnd',[0,0,0.6])
                shape.col = alpha*np.array(colorBegin)+(1-alpha)*np.array(colorEnd)

        elif self.colorOpts['colorId'] == self.linearMapColorId:
            if shape.shapeId in Shape.staticShapeIds.values(): #boundary/background
                shape.col = self.colorOpts.get('backgroundColor',[0.1,0.1,0.1])
            else:
                if 'colorList' in self.colorOpts:
                    colormap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',self.colorOpts['colorList'])
                else:
                    colorMapName = self.colorOpts.get('colorMapName','Set1') # alternative: rainbow
                    colormap = mpl.pyplot.cm.get_cmap(colorMapName)

                alpha = 1-exp(-shape.i/50)
                shape.col = colormap(alpha)#[0:3] # remove last value in colormap(...) (for black)        
            
    def recolor(self,colorOpts):
        '''change color options and recolor boundary and all shapes in shapelist

        Args:
            colorOpts: options for coloring

        Returns:
            None

        '''
        print('change colorId to %i' % colorOpts['colorId'])
        self.colorOpts = colorOpts
        self.makecolor(self.boundary)
        for shape in self.shapelist:
            self.makecolor(shape)