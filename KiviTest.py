 #!python


import numpy as np

from kivy.config import Config
Config.set('graphics', 'fullscreen', '0')

import kivy
from kivy.core.window import Window
from kivy.graphics import *
from kivy.uix.screenmanager import Screen,ScreenManager
from kivy.uix.colorpicker import ColorPicker 
from kivy.uix.button import Button
from kivy.uix.stacklayout import StackLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.lang import Builder
from kivy.properties import StringProperty 
from kivy.uix.scrollview import ScrollView

import abc


class ConverterError(Exception):
    """ 
        If conversion fails, this exceptions gets triggered, we might also have a bestValue
        available.
    """
    
    def __init__(self, converterType, message, bestValue):
        # Call the base class constructor with the parameters it needs
        super(ConverterError, self).__init__(message)
        self.converterType = converterType
        self.bestValue = bestValue
        
        
        
class TypeConverter(object, metaclass=abc.ABCMeta):
    """ 
        Base class for the type conversions.
    """
    
    def __init__(self, type ):
        self.__type = type # the name of the type (e.g "color")
    
    @property
    def type(self):  
        return self.__type
    
    @abc.abstractmethod
    def toType(self,string):
        raise NotImplementedError("Define function 'toType' to use this base class")
    
    @abc.abstractmethod
    def toString(self,value):
        raise NotImplementedError("Define function 'toString' to use this base class")
    
    def throwError(self,message,bestValue=None, postfix="failed"):
         raise ConverterError(self, message+" - ["+postfix+"]", bestValue)
        
    def throwErrorIf(self,condition,message,bestValue=None, postfix="failed"):
        if condition:
            raise ConverterError(self, message+" - ["+postfix+"]", bestValue)
    
    def throwErrorIfNotType(self,input,typ):
        if not isinstance(input,typ):
            self.throwError("TypeCheck:" + str(type(input)) + " != " + str(typ) )

import ast
class ColorConverter(TypeConverter):
    """
        A stupid color (4 dimensional list) converter.
        type = list()
        
        A numpy array would be nice but, setting a numpy array in the ColorPickerScreen.setColor()
        results in a error (kivy) 
    """
    def __init__(self):
        super(ColorConverter,self).__init__("color")
    
    def toType(self, string):
        self.throwErrorIfNotType(string,str)
        # parse string into a numpy array.
        try:
            color = np.ravel(np.array(ast.literal_eval(string),dtype=float))
        except (ValueError, SyntaxError) as e:
            self.throwError("""ColorConverter conversion 
                               error for string: %s""" % string, None)
        
        return self.checkValue(color)
    
    def toString(self,color):
        self.throwErrorIfNotType(color,(np.ndarray, list)) 
        if isinstance(color,list):
            return str(color)
        else:
            return str(list(color))
        
    def checkValue(self,color):
        # check dimension
        self.throwErrorIf(color.shape[0] != 4, "DimensionCheck", None)
        return np.clip(color, 0.0,1.0).tolist()
    

class IntRangeConverter(TypeConverter):
    """
        A stupid integral range converter.
        type = int()
    """
    def __init__(self):
        super(IntRangeConverter,self).__init__("intRange")
    
    def toType(self, string, min, max):
        self.throwErrorIfNotType(string,str)
        # parse string into a numpy array.
        try:
            number = int(string)
        except ValueError as e:
            self.throwError("""IntRangeConverter conversion 
                               error for string: %s""" % string)
            
        return self.checkValue(number,min,max)
    
    def toString(self,number):
        self.throwErrorIfNotType(number,int)   
        return str(number);
        
    def checkValue(self,number,min,max):
        return int(np.clip(number,min,max))
            
class ConvertManager:
    
    """
        The ConverterManager which manages conversion of values.
        You can register your own type converter here. 
    """
    
    def __init__(self):
        self.typeConverterMap = {}
        
    def registerType(self,typeConverter):
        
        if not isinstance(typeConverter,TypeConverter):
            raise ValueError("Converter",typeConverter, " not of type TypeConverter")
            
        if typeConverter.type not in self.typeConverterMap:
            self.typeConverterMap[typeConverter.type ] = typeConverter
        else:
            raise ValueError("Converter type: ", typeConverter.type, " already register!" )
    
    def unregisterType(self,type):
        if type in self.typeConverterMap:
            del self.typeConverterMap[type] 
    
    
    def convert(self, input, settingName, typeName , converterArgs={}, ):
        # dispatch to the underlying registered TypeConverter
        # if input is a string -> convert to type
        # if input is not a string -> convert to string
        
        if typeName not in self.typeConverterMap:
            raise ConverterError(TypeConverter, "Converter type: '%s' not registered in TypeConverter" % type);
        
        converter = self.typeConverterMap[typeName]
        
        try:

            # Try to call the converter with the arguments converterArgs
            if isinstance(input,str):
                return converter.toType(input, **converterArgs)
            else:
                return converter.toString(input)
            
        except ConverterError as c:
            
            raise ConverterError(type(converter),
                                 message="""Converter '%s' could not convert 
                                    input: %s, type: %s -> settingName: %s"""
                                 % (typeName,input,type(input), settingName),  bestValue=c.bestValue) from c
        except TypeError as t:
            
            raise ConverterError(type(converter), 
                                 message="""Converter '%s' could not be called
                                    for input: %s, type: %s  -> settingName: %s
                                    (possible wrong function signature)""" 
                                % (typeName,input,type(input), settingName), bestValue=None ) from t
            

class KeyValueWidget(GridLayout):
    
    """
        The widget which contains a Label and a TextInput field.
        
        The input in the TextInput is synchronized with self.settingProps["value"]
        by converting self.settingProps["value"]<->string with the self.converterManager
        self.settingProps["value"] has the same type as the output of the converter
    """
    
    def __init__(self,settingName, settingProps,convertManager, **kwargs):
        
        super(KeyValueWidget, self).__init__(**kwargs)
        
        # save the settingName in the TextInput instance
        self.settingName  = settingName 
        # save corresponding settingProps in the TextInput instance
        # (to later store back the value)
        self.settingProps = settingProps 

        self.convertManager = convertManager
    
        self.cols = 2
        
        self.addLabel()
        self.addInput()
    
    def _setNormalMode(self):
        self.textInput.background_color = (1,1,1,1)
    
    def _setHighlightedMode(self):
        self.textInput.background_color = (1,0,0,1)
    
    """     #settingName
            "color"
            
            #These are the "settingProps"
            { 
                "value" : [1,0,1],
                "labelName": "Color",
                "type" : "color"
            },
            
            # or 
            
            #settingName
            "maxWidth"
            
            #These are the "settingProps"
            {  
                "value" : 40,
                "labelName": "Maximum Width",
                "type": "intRange",
                "converterArgs": {min: 0, max: 100},
                "units": "[px]"
            }
        }
    """
    
    def _saveFromTextInput(self,*args):
        """ Writes the value in the TextInput,strri field to its settingProps."""
        # run the string through the converter to produce a non-string value
        try:
            converterArgs = {}
            if "converterArgs" in self.settingProps:
                converterArgs = self.settingProps["converterArgs"]

            value = self.convertManager.convert(self.textInput.text,
                                                self.settingName,
                                                self.settingProps["type"], 
                                                converterArgs)
            
            
            # write it back into the settingProps
            self.settingProps["value"] = value

            
        except ConverterError as c:
            print(c,"  Cause: ", c.__cause__,)
            #self._setHighlightedMode()
            if c.bestValue is not None:
                self.settingProps["value"] = c.bestValue
        
        finally:  
            self._resetTextInput()
            self._setNormalMode()            
        
        
    def _resetTextInput(self,*args):
        """ Sets the value in the TextInput field from the settingProps."""
        
        # run the string through the converter to produce a non-string value
        converterArgs = {}
        if "converterArgs" in self.settingProps:
            converterArgs = self.settingProps["converterArgs"]

        string = self.convertManager.convert(self.settingProps["value"],
                                             self.settingName,
                                             self.settingProps["type"], 
                                             converterArgs)
        self.textInput.text = string

    
    def addLabel(self):
        # add the label
        t = self.settingProps["labelName"] + (" [" + self.settingProps["units"] + "]" if "units" in self.settingProps else "")
        self.label = Label(text=t, bold=True, halign='right')
        self.add_widget(self.label)
    
    def addInput(self):
        # add the TextInput field
        self.textInput = TextInput( multiline=False)
        self._resetTextInput()
        self._setNormalMode()

        # bind on_text such that it saves back the type-converted string into the settingsDict 
        self.textInput.bind(on_text_validate = self._saveFromTextInput )

        self.add_widget(self.textInput)
    

class KeyColorWidget(KeyValueWidget):
    """
        The widget which contains a key label and a TextInput field and a colored Button
        The button click, switches to the "ColorPickerScreen" in self.colorPickerScr 
    """
    
    def __init__(self,keyValueScreen,colorPickerScr,**kwargs):
        super(KeyColorWidget,self).__init__(**kwargs)
        self.keyValueScreen = keyValueScreen;
        self.colorPickerScr = colorPickerScr
        
    #@overrides(KeyValueWidget)
    def _saveFromTextInput(self,*args):
        super(KeyColorWidget,self)._saveFromTextInput()
        self.colorButton.background_color = self.settingProps["value"]
    

        
    #@overrides(KeyValueWidget)
    def addInput(self):
        # add a TextInput and a Button
        g = GridLayout()
        g.cols=2
        
        # add the text field
        self.textInput = TextInput( multiline=False,size_hint_x=0.9)
        g.add_widget(self.textInput)
        
        self.colorButton= Button(background_color=self.settingProps["value"], 
                                 background_normal="",
                                size_hint_x=0.1)
        self.colorButton.bind(on_press = self.colorButtonCallback)
        g.add_widget(self.colorButton)
        
        self._resetTextInput()
        self._setNormalMode()
    
        # bind on_text such that it saves back the type-converted string into the settingsDict 
        self.textInput.bind(on_text_validate = self._saveFromTextInput )
        
        self.add_widget(g)
    
    def colorButtonCallback(self,instance):
        print("KeyColorWidget::colorButtonCallback")
        
        self.keyValueScreen.scm.transition.direction = "left"
        
        # set color in the color picker
        self.colorPickerScr.setColor(self.settingProps["value"])
        
        # set the color pick call back
        self.colorPickerScr.exclusivBindColor(self.colorPickCallback, backScreen = self.keyValueScreen.name );
        # switch to colorPicker
        self.colorPickerScr.switchToThis();
    
    def colorPickCallback(self,instance,value):
        print("KeyColorWidget::colorPickCallback", value)
        # set color
        self.settingProps["value"] = list(value) # to list, because color picker fives us some strange object
        self.colorButton.background_color = value
        # set value in text field
        self._resetTextInput()
        
        
class KeyValueScreen(Screen):
    
    def __init__(self,scm, settingsDict, convertManager,**kwargs):
        super(KeyValueScreen,self).__init__(name="KeyValueScreen",**kwargs)
        self.scm = scm
        self.convertManager = convertManager
        self.settingsDict = settingsDict
         
        self.scm.add_widget(self)
         
        # add a Color Picker Screen
        self.colorPickerScr = ColorPickerScreen(self.scm)
        
        # add a Screen
        #         -> ScrollView
        #             -> GridLayout
        #                  -> Widgets
        self.keyValueGrid = GridLayout(cols=1,spacing=5, size_hint_y=None, height=100)
        
        # when we add widgets to the grid and its minimum_height gets bigger, we always update the grids size like that!
        self.keyValueGrid.bind(minimum_height=self.keyValueGrid.setter('height'))
        
        self.scrollView = ScrollView()
        self.scrollView.add_widget(self.keyValueGrid)
        self.add_widget(self.scrollView)
        
        self.initSettings()
    
    def switchToThis(self):
        self.scm.current = "KeyValueScreen"
    
    def initSettings(self):
         # Add all settings to the grid
        for sett, props in sorted(self.settingsDict.items(),key=lambda x:x[1]["labelName"]):

            # all color types
            if props["type"] == "color":
                self.keyValueGrid.add_widget(
                    KeyColorWidget(keyValueScreen = self,
                                   colorPickerScr = self.colorPickerScr, 
                                   settingName=sett, settingProps=props, 
                                   convertManager=self.convertManager,
                                   size_hint_y=None, height=40))
             # all other key/value types 
            else:
                self.keyValueGrid.add_widget(
                    KeyValueWidget(settingName=sett, settingProps=props, 
                                   convertManager=self.convertManager,
                                   size_hint_y=None, height=40))
        

class ColorPickerScreen(Screen):
    def __init__(self,scm, **kwargs):
        super(ColorPickerScreen,self).__init__(name="ColorPicker",**kwargs)
        self.scm = scm
        self.backScreen="KeyValueScreen"
        self.scm.add_widget(self)
        
        g=GridLayout(cols=2)
        self.add_widget(g)
        
        self.backButton = Button(size_hint_x=0.1,text="Back")
        self.backButton.bind(on_press=self.backButtonCallback)
        g.add_widget(self.backButton)
        
        self.colorPicker = ColorPicker(size_hint_x=0.9)
        self.colorCallBack = None;
        g.add_widget(self.colorPicker)
        
    def switchToThis(self):
        self.scm.current = 'ColorPicker'
    
    def setColor(self,color):
        print("ColorPickerScreen::setColor",color, [type(t) for t in color])
        self.colorPicker.color = color
    
    def exclusivBindColor(self,callback, backScreen):
        
        print("ColorPickerScreen::exclusivBindColor: \n color changes go to: ",callback)
              
        # there is no widget.unbindAll("color") function which unbinds all
        # callbacks from event color.
        if self.colorCallBack is not None:
            self.colorPicker.unbind(color=self.colorCallBack)
            
        self.colorCallBack = callback
        self.colorPicker.bind(color=self.colorCallBack)
        
        self.backScreen = backScreen
        
    def backButtonCallback(self,instance):
        self.scm.transition.direction = "right"
        self.scm.current = self.backScreen

class SettingsScreenManager(ScreenManager):
    
    def __init__(self,settingsDict,**kwargs):
        
        super(SettingsScreenManager,self).__init__(**kwargs)
        
        self.settingsDicts = settingsDict
        
        # make a general converter
        self.convertManager = ConvertManager()
        self.convertManager.registerType(ColorConverter())
        self.convertManager.registerType(IntRangeConverter())
        
        # make the KeyValueScreen
        # this screen automatically adds 
        # the ColorPickerScreen as a second screen
        self.keyValueScr = KeyValueScreen(self,self.settingsDicts,
                                          self.convertManager,
                                          size_hint_y=1)
         
        self.keyValueScr.switchToThis()
        
from kivy.app import App
class MyApp(App):

    def build(self):
        
        self.gridL = GridLayout(cols=2)
        
        
        #=======================================================================
        # add some stupid widget with a simple  line in the first column
        #=======================================================================
        class MyWidget(Widget):
            pass
        # .kv string
        s = """
MyWidget:
    canvas:
        Color: 
            rgba: 1, .3, .8, .5
        Line:
            points: [[0,0],[100,100],[40,10],[500,100],self.size]
            width: 10
"""
                    
        w=Builder.load_string(s)
        self.gridL.add_widget(w)
        #=======================================================================

        
        #=======================================================================
        # add the SettingsManagerScreen
        #=======================================================================
        settingsDict = {
            #This is the "settingName"
            "color" : 
                        #These are the "settingProps"
                        { 
                            "value" : [1,0,1,1],
                            "labelName": "Color",
                            "type" : "color"
                        },
            
             "zColor" : 
                        #These are the "settingProps"
                        { 
                            "value" : [0,1,1,1], 
                            "labelName": "zColor",
                            "type" : "color"
                        },
            
            "maxWidth" : 
                        {  
                            "value" : 40,
                            "labelName": "Maximum Width",
                            "type": "intRange",
                            "converterArgs": {"min": 0, "max": 100},
                            "units": "px"
                        },
            
            "minWidth" :
                        {  
                            "value" : 10,
                            "labelName": "Minimum Width",
                            "type": "intRange",
                            "converterArgs": {"min": 0, "max": 100},
                            "units": "px"
                        },
            
            "backgroundColor" : 
                        {  
                            "value" : [1,0,0,1],
                            "labelName": "Background Color",
                            "type": "color",
                        },
            
            "labelId" : 
                        {  
                            "value" : 5,
                            "labelName": "LabelId",
                            "type": "intRange",
                            "converterArgs": {"min": 0, "max": 10},
                        },
                        
            #This is the "settingName"
            "color2" : 
                        #These are the "settingProps"
                        { 
                            "value" : [1,0,1,1],
                            "labelName": "Color",
                            "type" : "color"
                        },
            
            "zColor2" : 
                        #These are the "settingProps"
                        { 
                            "value" : [0,1,1,1], 
                            "labelName": "zColor",
                            "type" : "color"
                        },
            
            "maxWidth2" : 
                        {  
                            "value" : 40,
                            "labelName": "Maximum Width",
                            "type": "intRange",
                            "converterArgs": {"min": 0, "max": 100},
                            "units": "px"
                        },
            
            "minWidth2" :
                        {  
                            "value" : 10,
                            "labelName": "Minimum Width",
                            "type": "intRange",
                            "converterArgs": {"min": 0, "max": 100},
                            "units": "px"
                        },
            
            "backgroundColor2" : 
                        {  
                            "value" : [1,0,0,1],
                            "labelName": "Background Color",
                            "type": "color",
                        },
            
            "labelId2" : 
                        {  
                            "value" : 5,
                            "labelName": "LabelId",
                            "type": "intRange",
                            "converterArgs": {"min": 0, "max": 10},
                        }
        }
        
        # add some SettingScreen, which consists 
        # of a KeyValueScreen and a ColorPickerScreen (only for colors)
        self.sm = SettingsScreenManager(settingsDict, size_hint_x=None, width=300);
        self.gridL.add_widget(self.sm)
        #=======================================================================
        
        return self.gridL


if __name__ == '__main__':

     Window.size = (1000,600)
     MyApp().run()