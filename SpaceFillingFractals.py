#!python

import kivy

kivy.require('1.5.1')
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.stacklayout import StackLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.popup import Popup
from kivy.uix.scrollview import ScrollView
from kivy.uix.slider import Slider
from functools import partial

import matplotlib
matplotlib.use('Qt5Agg')

# Main class Fractal
from modules.fractal import Fractal

# import Shapes
from modules.shapes import *

# predefined parameter sets
from modules.parameterSets import parameterSetList

print('Possible designs are: ' + ' / '.join(parameterSetList.keys()))

class SomeWidget(BoxLayout):
    Shape = Shape
    parameterSetList = parameterSetList
    defaultDesign = 'circlesInChameleon'
    parameterSetList['']=parameterSetList[defaultDesign]
    def GO(self,button):
        print(button.state)
        if button.state == 'normal':
            self.myfractal.close()
            button.text = 'Go!'
        else:
            button.text = 'Close'
            # choose design and create instance of myfractal
            design=self.ids['design'].text#parameterSetList.keys():
            print(design)
            myfractal = Fractal(parameterSetList[design]['boundaryOpts'], parameterSetList[design]['colorOpts'],
                                **parameterSetList[design]['generalOpts'])

            nMax = parameterSetList[design]['generalOpts'].get('nMax', 30)
            nPushList = 4 * [1] + 3 * [2] + 4 * [5] + 2 * [10] + 5 * [20] + 9 * [50] + 10 * [100]

            for nPush in nPushList:
                nPush = min(nPush, nMax - myfractal.n)
                if not nPush: break
                for n in range(nPush):
                    print('\r\titeration: {}'.format(myfractal.n + 1), end=" ")
                    if (myfractal.push(**parameterSetList[design]['shapeOpts'])):
                        print('\n\tbreak during push of object number {}'.format(myfractal.n + 1))
                        myfractal.plot(n)
                        break
                else:
                    myfractal.plot(nPush)
                    continue
                break
            print('\r\titeration: {}'.format(myfractal.n))
            self.myfractal = myfractal
            self.running = 1


class ControlApp(App):
    def build(self):
        return SomeWidget()

if __name__ == '__main__':
 ControlApp().run()